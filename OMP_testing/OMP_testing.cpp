#include <iostream>
#include <chrono>
#include <vector>
#include <random>
#include <string>

void compute(std::vector<int>& vec) {
    
    for (int i = 0; i < vec.size(); i++)
        vec[i] += 3;
}

void computeOMP(std::vector<int>& vec) {

#pragma omp parallel for
    for (int i = 0; i < vec.size(); i++)
        vec[i] += 3;
}

void computeWithIterator(std::vector<int>& vec) {

    for (auto& r : vec)
        r += 3;
}

void computeWithIteratorOMP(std::vector<int>& vec) {


    for (std::vector<int>::iterator r = vec.begin(); r!= vec.end(); r++)
    #pragma omp single nowait
    {
        r += 3;
    }
     
}

int main()
{
    std::random_device rnd_device;
    // Specify the engine and distribution.
    std::mt19937 mersenne_engine{ rnd_device() };  // Generates random integers
    std::uniform_int_distribution<int> dist{ 1, 52 };

    auto gen = [&dist, &mersenne_engine]() {
        return dist(mersenne_engine);
    };

    std::vector<int> vec(3000000);
    generate(begin(vec), end(vec), gen);

    auto start = std::chrono::high_resolution_clock::now();

    compute(vec);

    auto end = std::chrono::high_resolution_clock::now();
    auto duration = (end - start);


    auto durationInMs = std::chrono::duration_cast<std::chrono::microseconds>(duration);

    std::string durationStr = "Time to run in series: " + std::to_string(durationInMs.count()) + " microseconds";

    std::cout << durationStr << std::endl;

    //***************************************************************************************
    auto start2 = std::chrono::high_resolution_clock::now();

    computeOMP(vec);

    auto end2 = std::chrono::high_resolution_clock::now();
    auto duration2 = (end2 - start2);


    auto durationInMs2 = std::chrono::duration_cast<std::chrono::microseconds>(duration2);

    std::string durationStr2 = "Time to run in parallel: " + std::to_string(durationInMs2.count()) + " microseconds";


    std::cout << durationStr2 << std::endl;


    //***************************************************************************************
    auto start3 = std::chrono::high_resolution_clock::now();

    computeWithIterator(vec);

    auto end3 = std::chrono::high_resolution_clock::now();
    auto duration3 = (end3 - start3);


    auto durationInMs3 = std::chrono::duration_cast<std::chrono::microseconds>(duration3);

    std::string durationStr3 = "Time to run in series with iterator: " + std::to_string(durationInMs3.count()) + " microseconds";


    std::cout << durationStr3 << std::endl;



    //***************************************************************************************
    auto start4 = std::chrono::high_resolution_clock::now();

    computeWithIteratorOMP(vec);

    auto end4 = std::chrono::high_resolution_clock::now();
    auto duration4 = (end4 - start4);


    auto durationInMs4 = std::chrono::duration_cast<std::chrono::microseconds>(duration4);

    std::string durationStr4 = "Time to run in series with iterator and omp: " + std::to_string(durationInMs4.count()) + " microseconds";


    std::cout << durationStr4 << std::endl;
}

